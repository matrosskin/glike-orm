var clientFactory = require('./connection/ClientFactory');
var EventEmitter = require('events').EventEmitter;
var _ = require('lodash');

var log = Log.getLogger();

var ee = new EventEmitter();
var domainList = {};
ee.on('tablePrepared', function(tableName) {
	log.debug('Checked table:', tableName);
	if (!domainList[tableName]) {
		log.error('Table "' + tableName + '" not registred for checking in DB');
		return new Error('Table "' + tableName + '" not registred for checking in DB');
	}

	delete domainList[tableName];

	if (!_.size(domainList)) {
		ee.emit('dbPrepared');
	}
});


function prepareDB(domains, fn) {
	//TODO: add asserts for incoming parameters

	_.each(domains, function(domain) {
		domainList[domain.tableName] = true;
	});

	ee.on('dbPrepared', fn);

	for (var domainName in domains) {
		var DomainClass = domains[domainName];

		clientFactory.getClient(function(err, client) {
			if (err) {
				log.error('Can\'t connect to DB.');
				return;
			}

			var fieldListToCheck = [];
			_.each(DomainClass.fields, function(fieldConf) {
				fieldListToCheck.push( fieldConf.name );
				// fieldList.push( _.template('<%= alias %>.<%= fieldName %>', {alias: alias, fieldName: fieldConf.name}) );
			});
			fieldListToCheck = ['id', 'name', 'text'];

			var jsonToSQL = require('./converters/jsonToSQL');
			var queryStr = jsonToSQL.select( {
				fields: [{name: 'table_name'}],
				schema: 'information_schema',
				// tableName: 'tables'
				tableName: 'columns'
			}, {
				'table_name': DomainClass.tableName,
				'table_schema': DomainClass.schema,
				$_in: { 'column_name': fieldListToCheck }
			} );

			client.query( queryStr, function(err, result) {
				if (err) {
					console.error(err);
					return;
				}

				if (!result || !result.rowCount) {
					log.debug( 'Domain "' + domainName + '" not exists in DB.' );
					client.query( jsonToSQL.createTable( DomainClass ), function(err, result) {
						console.log( 'createTable', err, result);
					} );
				} else if (result.rowCount == fieldListToCheck.length) {
					log.debug( 'Domain "' + domainName + '" exists with all columns in DB.' );
				} else if (result.rowCount < fieldListToCheck.length) {
					log.debug( 'Domain "' + domainName + '" not fully exists in DB.' );
					// TODO: implement adding of new column
				}

				ee.emit('tablePrepared', DomainClass.tableName);
			});
		});
	}
}

module.exports = prepareDB;
