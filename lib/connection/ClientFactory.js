var pg = require('pg');
var events = require('events');

var log = Log.getLogger();

var ConnectionStatus = require('./ConnectionStatus');

var conConfig = global.config.connectionConfig;

var ClientFactory = function() {
	this.doConnect();
}

ClientFactory.prototype = {
	status: ConnectionStatus.NOT_CONNECTED,
	client: null,
	events: new events.EventEmitter(),
	doConnect: function() {
		log.debug('Try to connect to DB.');
		this.status = ConnectionStatus.CONNECTING;
		var that = this;

		var client = new pg.Client(conConfig);
		client.connect(function(err) {
			if (err) {
				that.status = ConnectionStatus.ERROR;
				log.error('Connection error.', err);

				that.events.emit('end_connectiong');

				return;
			}

			that.status = ConnectionStatus.CONNECTED;
			that.client = client;

			that.events.emit('end_connectiong');
		});
	},
	getClient: function(callback) {
		log.debug('Try to get DB client. Current status:', this.status);
		var that = this;

		function callCallback() {
			var result = false;

			if (ConnectionStatus.CONNECTED == that.status) {
				log.debug('DB connected.');
				callback(null, that.client);
				result = true;
			}

			return result;
		}

		if (callCallback()) return;

		this.events.once('end_connectiong', function() {
			callCallback();
		});

		if (ConnectionStatus.NOT_CONNECTED == this.status || ConnectionStatus.ERROR == this.status) {
			this.doConnect();
		}
	}
};

module.exports = new ClientFactory();
