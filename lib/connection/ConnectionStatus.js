var ConnectionStatus = {
	NOT_CONNECTED: 'not connected',
	CONNECTING: 'connecting',
	CONNECTED: 'connected',
	ERROR: 'error'
};

module.exports = ConnectionStatus;
