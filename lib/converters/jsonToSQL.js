var _ = require('lodash');

var log = Log.getLogger();

var keyToLogic = {
	$_or: 'OR',
	$_and: 'AND'
};

var keyValueSqlTmpl = '<%= alias %>.<%= field %> <%= operator %> <%= value %>';

var keyToOperator = {
	$_eq: function(obj, alias) {
		var result = [];
		_.each( obj, function(val, key) {
			result.push( checkQueryKey( val, key, alias, function() {
				if ( _.isString(val) ) {
					val = "'" + val + "'";
				}

				return _.template( keyValueSqlTmpl, { alias: alias, field: key, value: val, operator: '='} );
			} ) );
		});

		return result;
	},
	$_gt: function(obj, alias) {
		var result = [];
		_.each( obj, function(val, key) {
			result.push( checkQueryKey( val, key, alias, function() {
				if ( _.isString(val) ) {
					val = "'" + val + "'";
				}

				return _.template( keyValueSqlTmpl, { alias: alias, field: key, value: val, operator: '>'} );
			} ) );
		});

		return result;
	},
	$_lt: function(obj, alias) {
		var result = [];
		_.each( obj, function(val, key) {
			result.push( checkQueryKey( val, key, alias, function() {
				if ( _.isString(val) ) {
					val = "'" + val + "'";
				}

				return _.template( keyValueSqlTmpl, { alias: alias, field: key, value: val, operator: '<'} );
			} ) );
		});

		return result;
	},
	$_in: function(obj, alias) {
		var result = [];
		_.each( obj, function(val, key) {
			result.push( checkQueryKey( val, key, alias, function() {
				if ( _.isString(val[0]) ) {
					val = "('" + val.join("', '") + "')";
				} else {
					val = '(' + val.join(', ') + ')';
				}

				return _.template( keyValueSqlTmpl, { alias: alias, field: key, value: val, operator: 'IN'} );
			} ) );
		});

		return result;
	}
};

function checkQueryKey(val, key, alias, fn) {
	var result = null;
	if (key.match(/^\$_/)) {
		key = key.toLowerCase();
		var subLogic = keyToLogic[key] || DEFAULT_LOGIC;
		var subOperator = key;

		var subResult = queryConvert( val, alias, subLogic, subOperator );
		if (keyToLogic[key]) {
			subResult = '(' + subResult + ')';
		}

		result = subResult;
	} else {
		result = fn();
	}

	return result;
}

var DEFEULT_ALIAS = '_this';
var DEFAULT_LOGIC = 'AND';
var DEFAULT_OPERATOR_KEY = '$_eq';

function queryConvert(jsonObj, alias, logic, operator) {
	alias = alias || DEFEULT_ALIAS;
	logic = logic || DEFAULT_LOGIC;
	operator = operator || DEFAULT_OPERATOR_KEY;

	var result = [];

	if ( _.isArray(jsonObj) ) {
		logic = keyToLogic['$_or'];
		_.each( jsonObj, function(subObj) {
			var subResult = queryConvert( subObj, alias, logic, operator );
			result.push( subResult );
		} );
	} else {
		result = result.concat( keyToOperator[operator]( jsonObj, alias ) );
	}

	return result.join(' ' + logic + ' ');
}

function selectQuery(DomainClass, queryJSON) {
	var alias = DEFEULT_ALIAS;

	var fieldList = [];
	_.each(DomainClass.fields, function(fieldConf) {
		fieldList.push( _.template('<%= alias %>.<%= fieldName %>', {alias: alias, fieldName: fieldConf.name}) );
	});

	var tmplData = {
		fieldList: fieldList.join(', ') || '*',
		schema: DomainClass.schema,
		table: DomainClass.tableName,
		alias: alias,
		query: queryConvert(queryJSON, alias) || 1
	};

	var sqlTmpl = 'SELECT <%= fieldList %> FROM <%= schema %>.<%= table %> AS <%= alias %> WHERE <%= query %>;'

	var selectFullQuerySql = _.template(sqlTmpl, tmplData);

	log.debug( 'Generated SQL for select query:', selectFullQuerySql );

	return selectFullQuerySql;
}

function createTableQuery(DomainClass) {
	var queryTmpl = 'CREATE TABLE <%= schema %>.<%= tableName %> ( <%= columnList %> );';

	var columnList = [];
	_.each( DomainClass.fields, function(field) {
		var type = field.type;
		if ( !type ) {
			type = 'text';
			if ( typeof field.default != 'undefined' && _.isNumber( field.default ) ) {
				type = 'integer';
			}
		}
		columnList.push(field.name + ' ' + type);
	} );

	var querySql = _.template( queryTmpl, {
		schema: DomainClass.schema,
		tableName: DomainClass.tableName,
		columnList: columnList.join(', ')
	} );

	log.debug( 'Generated SQL for create table query:', querySql );

	return querySql;
}

module.exports = {
	query: function(obj) {
		var result = queryConvert(obj);

		log.debug('Prepared SQL query:', result);

		return result;
	},
	select: selectQuery,
	createTable: createTableQuery
};
