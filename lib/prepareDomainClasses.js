var AbstractDomain = require('./abstract/AbstractDomain');
var util = require('util');
var _ = require('lodash');

var log = Log.getLogger();

function prepareDomainClasses(domains) {
	for (var domainName in domains) {
		var DomainClass = domains[domainName];
		util.inherits(DomainClass, AbstractDomain);

		DomainClass.prototype.fields = {};
		DomainClass.tableName = DomainClass.tableName || domainName.replace(/([a-z])([A-Z])/g, "$1_$2").toLowerCase();
		DomainClass.schema = DomainClass.schema || 'public';

		_.extend(DomainClass, AbstractDomain, function(dst, src) {
			if (!src) {
				return dst;
			}

			if (_.isObject(src)) {
				var result = _.clone(src);
				_.extend(result, dst);
				return result;
			}

			return src;
		});

		for (var field in DomainClass.fields) {
			log.debug('domain:', domainName, '; field,', field);
			var fieldName = field;
			var gsFieldName = fieldName.charAt(0).toUpperCase() + fieldName.slice(1); // name for getter and setter functions
			DomainClass.fields[field].name = DomainClass.fields[field].name || fieldName.replace(/([a-z])([A-Z])/g, "$1_$2").toLowerCase();

			(function(field) {
				var getField = DomainClass.prototype['get' + gsFieldName] || function() {
					log.debug('get field:', field);
					return this.fields[field];
				};
				var setField = DomainClass.prototype['set' + gsFieldName] || function(val) {
					log.debug('set field:', field);
					this.fields[field] = val;
				};

				Object.defineProperty( DomainClass.prototype, field, {
					get: getField,
					set: setField
				});
			})(field);
		}
	}
}

module.exports = prepareDomainClasses;
