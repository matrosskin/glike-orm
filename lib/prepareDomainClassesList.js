var fs = require('fs');

var log = Log.getLogger();

var domains = {};

function prepareDomains(path) {
	var domainsDirContent = fs.readdirSync(path);
	for (var fIndex in domainsDirContent) {
		var fileName = domainsDirContent[fIndex];
		var i = fileName.lastIndexOf('.');
		if (i < 0) { // TODO: may be it is neaded for additional checking that the file is directory.
			prepareDomains(path + '/' + fileName);
		} else {  // TODO: may be it is neaded for additional checking that the file is JavaScript.
			var domainClass = require(path + '/' + fileName);
			var domainName = fileName.substring(0, i);
			domains[domainName] = domainClass;
			log.info('Added to list domain name:', domainName);
		}
	}

	return domains;
}

module.exports = prepareDomains;
