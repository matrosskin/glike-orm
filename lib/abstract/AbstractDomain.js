var clientFactory = require('../connection/ClientFactory');
var assert = require('assert');

var log = Log.getLogger();

var AbstractDomain = function() {};

AbstractDomain.get = function(id, fn) {
	log.debug('Try to get instance with id:', id);
	assert(fn, 'callback is undefined.');
	assert(id, 'id is undefined.');

	// TODO: implement functionality
	log.error('Required implementation!!!');

	var result;
	var err = new Error();
	fn(err, result);
};

AbstractDomain.find = function(whereJSON, fn) {
	log.debug('Try to find instance by passed JSON:', whereJSON);
	assert(fn, 'callback is undefined.');
	assert(id, 'JSON is undefined.');

	// TODO: implement functionality
	log.error('Required implementation!!!');

	var result;
	var err = new Error();
	fn(err, result);
};

AbstractDomain.fields = {
	id: {
		type: 'INTEGER', 
		default: 0
	}
};

AbstractDomain.prototype = {
	save: function(fn) {
		log.debug('Try to save instance with id:', this.id);
		assert(fn, 'callback is undefined.');

		// TODO: implement functionality
		log.error('Required implementation!!!');

		log.debug('Save instance (id:', this.id, ') in to table:', this.__proto__.tableName);
		clientFactory.getClient(function() {
			var err = new Error();
			fn(err);
		});
	},
	delete: function(fn) {
		log.debug('Try to delete instance with id:', this.id);
		assert(fn, 'callback is undefined.');

		// TODO: implement functionality
		log.error('Required implementation!!!');

		log.debug('delete instance (id:', this.id, ') from table:', this.__proto__.tableName);
		clientFactory.getClient(function() {
			var err = new Error();
			fn(err);
		});
	}
};

module.exports = AbstractDomain;
