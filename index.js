var _ = require('lodash');
var fs = require('fs');
var EventEmitter = require('events').EventEmitter;

var log = Log.getLogger();
var ee = new EventEmitter();

var prepareDomainClassList = require('./lib/prepareDomainClassesList');
var config = global.config || {};
var domainsPath = config.domainsPath || config.appPath + '/domains';
log.info('Preparing domain list started.');
var domains = prepareDomainClassList(domainsPath);
log.info('Preparing domain list done.');

var prepareDomainClasses = require('./lib/prepareDomainClasses');
log.info('Preparing domain classes started.');
prepareDomainClasses(domains);
log.info('Preparing domain classes done.');

var prepareDB = require('./lib/prepareDB.js');
log.info('Preparing DB started.');
prepareDB(domains, function() {
	log.info('Preparing DB done.');
	ee.emit('ORMInitiated');

});

module.exports = ee;
